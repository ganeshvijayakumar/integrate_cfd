# INTEGRATE - CFD

This is a wrapper repository for all the codes used in CFD related tasks for the
INTEGRATE project. This repository is not designed to contain any major source
code by itself, but rather contain other repositories as git submodules and
provide scripts to automate compilation, installation and running tasks. For now
it helps compile the following software

### Mesh generation software 

Multiple software to create meshes around airfoils and other aerodynamic objects
in preparation for use by the HAMSTR solver

* [nearbodymesh](https://bitbucket.org/acstnbl/nearbodymesh/src/master/)
* [automesh](https://bitbucket.org/acstnbl/automesh/src/master/)
* [meshgen_beta](https://bitbucket.org/hamstr/meshgen_beta/src/master/)
* [delaundo](http://www.ae.metu.edu.tr/tuncer/ae546/prj/delaundo/)

### CFD solver

* [ham2d](https://bitbucket.org/hamstr/ham2d/src/master/) - 2D CFD solver for
  simulating flows around airfoils and other aerodynamic surfaces

 This software is licensed under TBD license. 

## Installation and Usage

[CMake](https://cmake.org) is used to compile and install all the required
software. This software has the following dependencies that are handled through
required packages in cmake

* [GNU compiler suite](https://gcc.gnu.org)
* [CGNS](https://cgns.github.io) - Mesh I/O file formats
* [METIS](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview) - Mesh
  partitioning software
* MPI - Message Passing Interface
* Python3 with development headers
* [mpi4py](https://mpi4py.readthedocs.io) - Python bindings for MPI
* [swig](http://www.swig.org) - Python - C/C++ interface software

This software will compile and install
[delaundo](http://www.ae.metu.edu.tr/tuncer/ae546/prj/delaundo/), a
triangulation software. However, you will need to manually download and unzip
the archive from
[here](http://www.ae.metu.edu.tr/tuncer/ae546/prj/delaundo/delaundo.zip) into
`src/delaundo` for now.

Configure, compile and install the software as follows

``` shell
git clone git@bitbucket.org:ganeshvijayakumar/integrate_cfd.git
cd integrate_cfd
mkdir build
cd build
cmake -DMETIS_DIR=MY_METIS_DIR -DCGNS_DIR=MY_CGNS_DIR -DCMAKE_INSTALL_PREFIX=MY_INSTALL_DIR ../
make -j18
make install
```

## Acknowledgements

The software compiled by this repository is developed by [Dr. James
Baeder](https://aero.umd.edu/clark/faculty/5/James-Baeder) and his research
group including Yong Su Jung and Miranda Constenoble at the University of
Maryland. The wrapper compilation scripts were developed by researchers at
[NREL](https://www.nrel.gov) with funding from DOE's ARPA-E research program
INTEGRATE.
