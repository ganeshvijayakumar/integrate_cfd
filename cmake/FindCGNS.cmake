# Find CGNS mesh datastructure library
#
# Set CGNS_DIR to the base directory where the package is installed
#
# Sets two variables
#   - CGNS_INCLUDE_DIRS
#   - CGNS_LIBRARIES
#

find_path(CGNS_INCLUDE_DIR
  cgnslib.h
  HINTS ${CGNS_DIR} ${CMAKE_INSTALL_PREFIX}
  PATH_SUFFIXES include)

find_library(CGNS_LIBRARY
  NAMES cgns
  HINTS ${CGNS_DIR} ${CMAKE_INSTALL_PREFIX}
  PATH_SUFFIXES lib)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  CGNS DEFAULT_MSG CGNS_INCLUDE_DIR CGNS_LIBRARY)
mark_as_advanced(CGNS_INCLUDE_DIR CGNS_LIBRARY)
