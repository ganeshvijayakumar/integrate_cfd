# Find METIS mesh decomposition library
#
# Set METIS_DIR to the base directory where the package is installed
#
# Sets two variables
#   - METIS_INCLUDE_DIR
#   - METIS_LIBRARY
#

find_path(METIS_INCLUDE_DIR
  metis.h
  HINTS ${METIS_DIR} ${CMAKE_INSTALL_PREFIX}
  PATH_SUFFIXES include)

find_library(METIS_LIBRARY
  NAMES metis
  HINTS ${METIS_DIR} ${CMAKE_INSTALL_PREFIX}
  PATH_SUFFIXES lib)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  METIS DEFAULT_MSG METIS_INCLUDE_DIR METIS_LIBRARY)
mark_as_advanced(METIS_INCLUDE_DIR METIS_LIBRARY)
