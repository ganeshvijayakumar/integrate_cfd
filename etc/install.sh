INTEGRATE_DIR=/projects/integrate/tmp/

mkdir ${INTEGRATE_DIR}/src
cd ${INTEGRATE_DIR}/src

#INSTALL CGNS first
git clone https://github.com/CGNS/CGNS
cd CGNS
mkdir build
cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=${INTEGRATE_DIR}/install/cgns/ -DCGNS_ENABLE_64BIT=OFF &> log.cmake
nice -n10 make -j18
make install
cd ../../

#INSTALL METIS
wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz
tar -zxf metis-5.1.0.tar.gz
cd metis-5.1.0
CC=gcc CXX=g++ make config prefix=${INTEGRATE_DIR}/install/metis &> log.config
nice -n10 make -j18 &> log.make
make install
cd ../../

#INSTALL integrate_cfd
git clone --recursive git@bitbucket.org:ganeshvijayakumar/integrate_cfd.git
cd integrate_cfd/src/nearbodymesh/nearbodymesh/
git apply ../../../etc/nearbody_mesh_patch
cd ../../../
mkdir build-gcc
cd build-gcc
cmake -DCGNS_DIR=${INTEGRATE_DIR}/install/cgns/ -DMETIS_DIR=${INTEGRATE_DIR}/install/metis/ -DCMAKE_INSTALL_PREFIX=${INTEGRATE_DIR}/install/integrate-cfd/ ../
nice -n10 make -j18
make install



