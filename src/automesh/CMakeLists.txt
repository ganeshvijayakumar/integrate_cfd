set(CELLTRIM_NEW_SOURCES
  automesh/cellTrim_new/src/trimMesh.C
  automesh/cellTrim_new/src/readInputs.C
  automesh/cellTrim_new/src/strandConn.C
  automesh/cellTrim_new/src/connectivity.C
  automesh/cellTrim_new/src/cellBlanking.C
  automesh/cellTrim_new/src/postprocStrandOps.C
  automesh/cellTrim_new/src/boundaries.C
  automesh/cellTrim_new/src/outputs.C
  automesh/cellTrim_new/src/destructors.C
  automesh/cellTrim_new/src/sparse.C
  automesh/cellTrim_new/src/polygon.C)

add_executable(celltrim_new
  ${CELLTRIM_NEW_SOURCES})

target_compile_options(celltrim_new
  PRIVATE $<$<COMPILE_LANGUAGE:CXX>:${INTEGRATE_CFD_WARN_FLAGS}>)

install(TARGETS celltrim_new
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib)



set(CREATE_HYBRIDMESH_SOURCES
  automesh/createHybridMesh/src/dpl2tec.C
  automesh/createHybridMesh/src/main.C
  automesh/createHybridMesh/src/readInputs.C
  automesh/createHybridMesh/src/stitch.C
  automesh/createHybridMesh/src/writeDelaundoInput.C
  automesh/createHybridMesh/src/writeOutput.C)

add_executable(create_hybridmesh
  ${CREATE_HYBRIDMESH_SOURCES})

target_compile_options(create_hybridmesh
  PRIVATE $<$<COMPILE_LANGUAGE:CXX>:${INTEGRATE_CFD_WARN_FLAGS}>)

install(TARGETS create_hybridmesh
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib)

install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/automesh/createHybridMesh/cases/autorun/delaundo/delaundo.ctr
  DESTINATION etc)


set (HAMSTRAN_SOURCES
  automesh/hamstran/Merging_stitchNB/src_cpp/hamstran.C
  automesh/hamstran/Merging_stitchNB/src_cpp/readInputs.C
  automesh/hamstran/Merging_stitchNB/src_cpp/edgeConn.C
  automesh/hamstran/Merging_stitchNB/src_cpp/classification.C
  automesh/hamstran/Merging_stitchNB/src_cpp/transform.C
  automesh/hamstran/Merging_stitchNB/src_cpp/merging.C
  automesh/hamstran/Merging_stitchNB/src_cpp/merging_blind.C
  automesh/hamstran/Merging_stitchNB/src_cpp/stitching_v2.C
  automesh/hamstran/Merging_stitchNB/src_cpp/writeOutputs.C
  )

add_executable(hamstran
  ${HAMSTRAN_SOURCES})

target_compile_options(hamstran
  PRIVATE $<$<COMPILE_LANGUAGE:CXX>:${INTEGRATE_CFD_WARN_FLAGS}>)

install(TARGETS hamstran
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib)

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/automesh_run.py.in ${CMAKE_CURRENT_BINARY_DIR}/automesh_run.py)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/automesh_run.py
  DESTINATION bin)
