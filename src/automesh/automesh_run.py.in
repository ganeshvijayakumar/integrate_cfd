# Script for automating meshing tasks, testing with Centennial HPC system configuration
#
# Set up like qq script, but using new cell trimming method

import os
import glob
import subprocess
import shutil
#import matlab.engine
#import time

lod_nearbodymesh = ['nearBodyMesh','nearBodyMesh/grid','nearBodyMesh/structuredMeshes','nearBodyMesh/structuredMeshes/surfaceData','nearBodyMesh/structuredMeshes/hamstr', 'nearBodyMesh/structuredMeshes/hamstr/sparse']
lod_celltrimnew = ['cellTrim_new','cellTrim_new/grid','cellTrim_new/output','cellTrim_new/output/tecplot']
lod_createhybridmesh = ['createHybridMesh','createHybridMesh/cases','createHybridMesh/cases/autorun','createHybridMesh/cases/autorun/input','createHybridMesh/cases/autorun/delaundo']
lod_hamstran = ['hamstran','hamstran/Merging_stitchNB','hamstran/Merging_stitchNB/input']
lod_meshgen = ['meshgen_test','meshgen_test/autorun','meshgen_test/autorun/input','meshgen_test/autorun/QuadData']
lod = lod_nearbodymesh + lod_celltrimnew + lod_createhybridmesh + lod_hamstran + lod_meshgen

for d in lod:
    if ( not os.path.isdir(d) ):
        os.mkdir(d)

for fl in (glob.glob('*txt') + glob.glob('*dat')):
    print("nearBodyMesh/"+fl)
    shutil.copy(fl,"nearBodyMesh/"+fl)

## Cleanup of nearBodyMesh ##
os.chdir("./nearBodyMesh/grid")
for fl in glob.glob("*.plt"):
    os.remove(fl)
os.chdir("./../structuredMeshes")
for fl in glob.glob("*/*.dat"):
    os.remove(fl)
for fl in glob.glob("./hamstr/TecPlot/*.plt"):
    os.remove(fl)
os.chdir("./../..")

## Cleanup of cellTrim ##
for fl in glob.glob("./cellTrim_new/*.dat"):
    os.remove(fl)
for fl in glob.glob("./cellTrim_new/grid/*.plt"):
    os.remove(fl)
for fl in glob.glob("./cellTrim_new/output/*.dat"):
    os.remove(fl)
for fl in glob.glob("./cellTrim_new/output/tecplot/*"):
    os.remove(fl)

## Cleanup of createHybridMesh inputs ##
for fl in glob.glob("./createHybridMesh/cases/autorun/input/*.dat"):
    os.remove(fl)

## Cleanup of hamstran inputs ##
for fl in glob.glob("./hamstran/Merging_stitchNB/input/*.dat"):
    os.remove(fl)
for fl in glob.glob("./hamstran/Merging_stitchNB/*.dat"):
    os.remove(fl)

## Cleanup of hamstr meshgen inputs ##
for fl in glob.glob("./meshgen_test/autorun/input/*.dat"):
    os.remove(fl)

print("Running nearbody mesh")
## Run near-body mesh ##
os.chdir("./nearBodyMesh")
subprocess.call(["srun","${CMAKE_INSTALL_PREFIX}/bin/nearbodymesh"])
os.chdir("./..")

## Move files needed for cellTrim ##
for fl in glob.glob("./nearBodyMesh/grid/*.plt"):
    shutil.copy(fl,"./cellTrim_new/grid/")

## Execute cell trimming ##
os.chdir("./cellTrim_new")
# Check number of elements
fns=os.listdir("./grid")
nelm=len(fns)
nelms=str(nelm)

print("nelm = ", nelm)
print("Running celltrim_new")
# Run actual programs
subprocess.call(["srun","${CMAKE_INSTALL_PREFIX}/bin/celltrim_new",nelms,nelms])
os.chdir("./..")

## Move files needed to generate hybrid mesh ##
for fl in glob.glob("./cellTrim_new/output/*.dat"):
    shutil.copy(fl,"./createHybridMesh/cases/autorun/input/")

shutil.copy("${CMAKE_INSTALL_PREFIX}/etc/delaundo.ctr","./createHybridMesh/cases/autorun/delaundo")
shutil.copy("${CMAKE_INSTALL_PREFIX}/bin/delaundo","./createHybridMesh/cases/autorun/delaundo")

print("Running create_hybridmesh")
## Run hybrid meshgen ##
os.chdir("./createHybridMesh/cases/autorun/")
subprocess.call(["srun","${CMAKE_INSTALL_PREFIX}/bin/create_hybridmesh"])

## Copy files for HAMSTRAN ##
shutil.copy("conn_fine.dat","../../../hamstran/Merging_stitchNB/input/conn0.dat");
shutil.copy("conn0.dat","../../../hamstran/Merging_stitchNB/input/conn_in.dat");
shutil.copy("coord_fine.dat","../../../hamstran/Merging_stitchNB/input/coord0.dat");
shutil.copy("coord0.dat","../../../hamstran/Merging_stitchNB/input/coord_in.dat");
shutil.copy("sparse2normal.dat","../../../hamstran/Merging_stitchNB/input/sparse2normal0.dat");
os.chdir("./../../..")
os.remove("createHybridMesh/cases/autorun/delaundo/delaundo")

## Execute HAMSTRAN ##
print("Running hamstran")
os.chdir("./hamstran/Merging_stitchNB")
subprocess.call(["srun","${CMAKE_INSTALL_PREFIX}/bin/hamstran"])
os.chdir("../../")

## Copy files for hamstr ##
shutil.copy("./hamstran/Merging_stitchNB/conn0.dat","./meshgen_test/autorun/input/conn0.dat")
shutil.copy("./hamstran/Merging_stitchNB/coord0.dat","./meshgen_test/autorun/input/coord0.dat")
shutil.copy("./hamstran/Merging_stitchNB/normal0.dat","./meshgen_test/autorun/input/normal0.dat")
shutil.copy("./hamstran/Merging_stitchNB/mesh0.dat","./meshgen_test/autorun/input/mesh0.dat")
shutil.copy("${CMAKE_INSTALL_PREFIX}/etc/input.nearbody","./meshgen_test/autorun/input.nearbody")

## Execute hamstr meshgen ##
nproc=1;
nprocs=str(nproc);
os.chdir("./meshgen_test/autorun")
subprocess.call(["srun","-n",nprocs,"python","${CMAKE_INSTALL_PREFIX}/bin/xrun.py"])
os.chdir("./../..")
