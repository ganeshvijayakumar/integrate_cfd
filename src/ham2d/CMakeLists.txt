set(HAM2D_SOURCES
  ham2d/src/ham2d.c
  ham2d/src/readGrid.c
  ham2d/src/preprocess.c
  ham2d/src/find_faces.c
  ham2d/src/nodebase.c
  ham2d/src/initflow.c
  ham2d/src/stepSolution.c
  ham2d/src/updateSoln.c
  ham2d/src/communication.c
  ham2d/src/communication_node.c
  ham2d/src/communicationLinear.c
  ham2d/src/computeRHS.c
  ham2d/src/computeTimeScaling.c
  ham2d/src/computeLinearRHS.c
  ham2d/src/flux_roe_2d.c
  ham2d/src/periodic_bc.c
  ham2d/src/apply_periodic.c
  ham2d/src/apply_periodic_LHS.c
  ham2d/src/muscld.c
  ham2d/src/muscld_deriv.c
  ham2d/src/roeflx.c
  ham2d/src/computeForce.c
  ham2d/src/outputSolution.c
  ham2d/src/wrest.c
  ham2d/src/rrest.c
  ham2d/src/smoothGrid.c
  ham2d/src/jac_roe_2d.c
  ham2d/src/flux_visc.c
  ham2d/src/triSolvers.c
  ham2d/src/mathops.c
  ham2d/src/findDiagonals.c
  ham2d/src/ADI.c
  ham2d/src/DADI.c
  ham2d/src/gaussSeidel.c
  ham2d/src/lineGaussSeidel.c
  ham2d/src/lineGaussSeidel_single.c
  ham2d/src/weno.c
  ham2d/src/weno_deriv.c
  ham2d/src/crweno.c
  ham2d/src/crweno_z.c
  ham2d/src/crteno5.c
  ham2d/src/tri.c
  ham2d/src/wallshear.c
  ham2d/src/turvis.c
  ham2d/src/turfluxr2.c
  ham2d/src/turdiffs2.c
  ham2d/src/tursource2.c
  ham2d/src/turfluxja.c
  ham2d/src/io.c
  ham2d/src/length_scale.c
  ham2d/src/readGridmotion.c
  ham2d/src/grdvel.c
  ham2d/src/findConnections.c
  ham2d/src/new_preprocess.c
  ham2d/src/jac_visc_2d.c
  ham2d/src/grid_metric.c
  ham2d/src/fourth_central.c
  ham2d/src/second_central.c
  ham2d/src/edge_update.c
  ham2d/src/satrans.c
  ham2d/src/sa_gammathetaflux.c
  ham2d/src/sa_gammathetasource.c
  ham2d/src/sa_gammathetaja.c
  ham2d/src/walldis.c
  ham2d/src/walldis_exact.c
  ham2d/src/flatplate.c
  ham2d/src/bumpcase.c
  ham2d/src/communication_residual.c
  ham2d/src/communication_residual_gmres.c
  ham2d/src/komegasst.c
  ham2d/src/komega_ja.c
  ham2d/src/komega_convec.c
  ham2d/src/komega_diffus.c
  ham2d/src/komega_source.c
  ham2d/src/komega_findmut.c
  ham2d/src/DADI_SA.c
  ham2d/src/DDLGS_SA.c
  ham2d/src/gmres.c
  ham2d/src/computeRHSGeneral.c
  ham2d/src/utilsgmres.c
  ham2d/src/precondgmres.c
  ham2d/src/communication_gmres.c
  ham2d/src/flux_visc_gmres.c
  ham2d/src/muscld_SA.c
  ham2d/src/ADI_GMRES.c
  ham2d/src/DDLGS_GMRES.c
  ham2d/src/DDLGS_GMRES_single.c
  ham2d/src/PGS_GMRES.c
  ham2d/src/computeRHSUnified.c
  ham2d/src/computeRHSUnified_Urecon.c
  ham2d/src/computeRHSUnified_Urecon_Mix.c
  ham2d/src/pre_samodel.c
  ham2d/src/pre_komegasst.c
  ham2d/src/pre_satrans.c
  ham2d/src/teno6pts.c
  ham2d/src/weno6pts.c
  ham2d/src/teno5pts.c
  ham2d/src/nodebase_gmres.c
  ham2d/src/lls.c
  ham2d/src/flux_visc_lls.c
  ham2d/src/generate_ghostcell_wall.c
  ham2d/src/generate_ghostcell_mpi.c
  ham2d/src/fill_ghost.c
  ham2d/src/least_square.c
  ham2d/src/flux_visc_least.c
  ham2d/src/fill_ghost_gmres.c
  ham2d/src/umetric.c
  ham2d/src/crteno6.c
  ham2d/src/roughness_amplitude.c
  ham2d/src/roughness_amplitude_flux.c
  ham2d/src/roughness_amplitude_ja.c
  ham2d/src/pre_roughness.c
  ham2d/src/lls_cell.c
  ham2d/src/lls_cell_4neigh.c
  ham2d/src/lls_umuscl.c
  ham2d/src/limiter.c
  ham2d/src/umuscld.c
  ham2d/src/checklooptype.c
  ham2d/src/multielement.c
  ham2d/src/pre_sa_gamma.c
  ham2d/src/sa_gamma_flux.c
  ham2d/src/sa_gamma_source.c
  ham2d/src/sa_gamma_ja.c
  ham2d/src/sa_gamma.c
  ham2d/src/boundaryLayer_v2.c
  ham2d/src/check_exit.c
  ham2d/src/computeForce_vis.c
  ham2d/src/DDLGS_SST.c
  ham2d/src/triSolvers_LU.c
  ham2d/src/lineGaussSeidel_single_trape.c
  ham2d/src/lineGaussSeidel_single_bdf2.c)

find_package(MPI REQUIRED)

include_directories(SYSTEM ${MPI_INCLUDE_PATH})

add_executable(ham2d
  ${HAM2D_SOURCES})

if(MPI_COMPILE_FLAGS)
  set_target_properties(ham2d PROPERTIES
    COMPILE_FLAGS "${MPI_COMPILE_FLAGS}")
endif(MPI_COMPILE_FLAGS)
target_compile_options(ham2d
  PRIVATE $<$<COMPILE_LANGUAGE:CXX>:${INTEGRATE_CFD_WARN_FLAGS}>)

target_link_libraries(ham2d m MPI::MPI_C)
if(MPI_LINK_FLAGS)
  set_target_properties(ham2d PROPERTIES
    LINK_FLAGS "${MPI_LINK_FLAGS}")
endif(MPI_LINK_FLAGS)

install(TARGETS ham2d
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib)
